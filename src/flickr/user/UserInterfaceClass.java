/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr.user;

import flickr.FlickrConnection;
import flickr.FlickrFunctions;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.io.CopyStreamAdapter;
import flickr.subpackage.ProgressBar;
import java.net.SocketException;
import flickr.user.UserPhotosList;

/**
 *
 * @author Oz
 */
public class UserInterfaceClass {

	FlickrConnection fConnection;
	FTPClient ftpClient = null;
	File file;
	InputStream targetStream;
	ProgressBar bar;
	public int fileCounter;
	public boolean COMPLETE = false;
	JButton button;
	FlickrFunctions fObj = new FlickrFunctions ();

	public UserInterfaceClass ( FlickrConnection fConnection ) {
		this.fConnection = fConnection;
	}

	public UserInterfaceClass ( FlickrConnection fConnection, ProgressBar bar, JButton button ) {
		this.fConnection = fConnection;
		this.bar = bar;
		this.bar.setStringPainted ( true );
		this.bar.setValue ( 0 );
		this.button = button;
	}
	/*
	 boolean checkFileExists ( String filePath ) {
	 if ( ftpClient == null ) {
	 try {
	 fConnection.getFTPclient ();
	 } catch ( SocketException ex ) {
	 fObj.PrintDialog ( "Impossibile stabilire la connessione ftp", ex.getMessage () );
	 System.out.println ( ex.getMessage () );
	 Logger.getLogger ( UserInterfaceClass.class.getName () ).log ( Level.SEVERE, null, ex );
	 }
	 }
	 try {
	 InputStream inputStream = ftpClient.retrieveFileStream ( filePath );
	 int returnCode = ftpClient.getReplyCode ();
	 if ( inputStream == null || returnCode == 550 ) {
	 return false;
	 }
	 } catch ( IOException ex ) {
	 fObj.PrintDialog ( "Io exception", ex.getMessage () );
	 System.out.println ( ex.getMessage () );
	 Logger.getLogger ( UserInterfaceClass.class.getName () ).log ( Level.SEVERE, null, ex );
	 }

	 return true;
	 }
	 */

	/**
	 * converte il file in oggetto Stream per il caricamento su ftp
	 *
	 * @param file
	 * @return
	 */
	public InputStream fileToStream ( File file ) {
		InputStream targetStream = null;
		try {

			targetStream = new FileInputStream ( file );
			System.out.println ( "File selezionato: " + file.getName () );

		} catch ( FileNotFoundException ex ) {
			Logger.getLogger ( UserInterfaceClass.class.getName () ).log ( Level.SEVERE, null, ex );
		}

		return targetStream;
	}

	/**
	 * caricamento file su ftp
	 *
	 * @param path
	 * @param ftpClient
	 * @param targetStream
	 * @param file
	 */
	public void ftpUploadSingle ( String path, FTPClient ftpClient, InputStream targetStream, final File file, UserPhotosList father ) {
		try {
			this.file = file;
			System.out.println ( "percorso remoto: " + path );
			this.targetStream = targetStream;
			//cambia la directory di lavoro su ftp
			ftpClient.changeWorkingDirectory ( path );
			//modifica il buffer di caricamento
			ftpClient.setBufferSize ( 8192 );
			System.out.println ( "buffer:" + ftpClient.getBufferSize () );
			//verifica che la directory esista, altrimenti la crea
			int returnCode = ftpClient.getReplyCode ();
			if ( returnCode == 550 ) {
				ftpClient.makeDirectory ( path );
				ftpClient.changeWorkingDirectory ( path );
			}

			/**
			 * aggiorna lo stato del caricamento mostrato all'utente del file su
			 * server
			 */
			CopyStreamAdapter streamListener = new CopyStreamAdapter () {
				@Override
				public void bytesTransferred ( long totalBytesTransferred, int bytesTransferred, long streamSize ) {
					int percent = (int) (totalBytesTransferred * 100 / file.length ());
					bar.setValue ( percent );
					String bytes = Long.toString ( totalBytesTransferred / 1024 ) + " di  " + file.length () / 1024 + "KBytes";
					bar.setString ( bytes );
				}

			};
			//imposta il listener che aggiorna lo stato del caricamento mostrato all'utente
			ftpClient.setCopyStreamListener ( streamListener );

			if ( targetStream != null ) {
				System.out.println ( "File in caricamento...." );
				bar.setIndeterminate ( false );
				bar.setVisible ( true );

				bar.newThread ();
				bar.setValue ( 0 );

				//avvia il caricamento su ftp in un thread separato
				new Thread ( new FtpRunner ( ftpClient, file, targetStream, father ) ).start ();
				bar.stopProgress ();

			}
		} catch ( IOException ex ) {
			System.out.println ( ex.getMessage () );
			Logger.getLogger ( UserInterfaceClass.class.getName () ).log ( Level.SEVERE, null, ex );
		} finally {
			COMPLETE = true;

		}
	}

	/**
	 * cancellazione file da ftp
	 *
	 * @param ftpClient
	 * @param mediaID
	 * @param userID
	 * @param path
	 */
	public void ftpDeleteSingle ( FTPClient ftpClient, int mediaID, int userID, String path ) {

		path = path.replace ( "flickrdb.altervista.org", "" );

		System.out.println ( "Percorso remoto: " + path );
		try {
			boolean deleted = ftpClient.deleteFile ( path );
			if ( deleted ) {
				System.out.println ( "Ftp: File cancellato correttamente." );
			} else {
				System.out.println ( "Ftp: Impossibile cancellare il file." );
			}
		} catch ( IOException ex ) {
			System.out.println ( "Ftp: Errore di IO: " + ex.getMessage () );
		}
	}

	/**
	 * thread background caricamento ftp
	 */
	private class FtpRunner implements Runnable {

		FTPClient ftpClient;
		File file;
		InputStream targetStream;
		UserPhotosList father;

		public FtpRunner ( FTPClient ftpClient, File file, InputStream targetStream, UserPhotosList father ) {
			this.ftpClient = ftpClient;
			this.file = file;
			this.targetStream = targetStream;
			this.father = father;
		}

		/**
		 * avvia il thread ed il caricamento
		 */
		public void run () {
			try {
				String tmp = button.getText ();
				button.setText ( "working..." );
				button.setEnabled ( false );
				ftpClient.storeFile ( file.getName (), targetStream );
				System.out.println ( "Upload completo" );
				fObj.PrintDialog ( "caricamento foto", "Inserimento avvenuto con successo." );
				button.setText ( tmp );
				fConnection.ftpCloseConnection ();
				button.setEnabled ( true );
				if ( father != null ) {
					father.execute ();
				}
			} catch ( IOException ex ) {
				Logger.getLogger ( UserInterfaceClass.class.getName () ).log ( Level.SEVERE, null, ex );
			}
		}

	}
}
