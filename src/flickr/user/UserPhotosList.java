/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flickr.user;

import flickr.DBTableModel;
import flickr.FQueries;
import flickr.FlickrConnection;
import flickr.FlickrFunctions;
import flickr.subpackage.test.TestQueryTable;
import java.math.BigDecimal;
import java.net.SocketException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;

/**
 *
 * @author ozeta
 */
public class UserPhotosList extends ToolBar {

	FlickrFunctions fObj = new FlickrFunctions ();
	FQueries fQuery;
	Statement stmt = null;
	ResultSet rset = null;

	/**
	 * Creates new form UserPhotosList
	 */
	public UserPhotosList ( FlickrConnection conn, int ID ) {
		super.fConnection = conn;
		this.fConnection = conn;
		fQuery = new FQueries ( conn );
		this.userID = ID;
		initComponents ();
		jTable1.setVisible ( false );
		setPanelTitle ( "Lista media" );
		execute ();

	}

	protected final void execute () {
		try {

			DBTableModel dataModel = fQuery.getMediaListFromUser ( userID );

			if ( dataModel != null ) {

				jTable1.setModel ( dataModel );
				jTable1.removeColumn ( jTable1.getColumnModel ().getColumn ( 0 ) );

				jScrollPane1.setVisible ( true );
				jTable1.setVisible ( true );
			}

		} catch ( SQLException ex ) {
			Logger.getLogger ( UserPhotosList.class.getName () ).log ( Level.SEVERE, null, ex );
			fObj.PrintDialog ( ex.getMessage () );
		} catch ( NullPointerException ex ) {
			//noConnection ();
		}
	}

	@SuppressWarnings ("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton4 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jButton4.setText("Visualizza Dettagli");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel1.setText("Filtra su titolo o descrizione");

        jTextField1.setText("titolo");

        jButton1.setText("Filtra");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cancella selezionato");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Apri immagine nel browser");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/flickr/res/24px/file112.png"))); // NOI18N
        jButton6.setText("Carica nuovo media");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(160, 160, 160)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 73, Short.MAX_VALUE)
                .addComponent(jButton6)
                .addGap(77, 77, 77)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(48, 48, 48))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(348, 348, 348)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton4)
                            .addComponent(jButton6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton3))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1)))
                .addContainerGap(150, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
/**
	 * visualizza i dettagli della foto
	 *
	 * @param evt
	 */
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
		int sel_row = jTable1.getSelectedRow ();
		int media_id;
		if ( sel_row != -1 ) {

			Statement stmt1 = null;

			BigDecimal test = (BigDecimal) jTable1.getModel ().getValueAt ( jTable1.getSelectedRow (), 0 );
			media_id = Integer.valueOf ( test.intValue () );
			System.out.println ( "media id= " + media_id );
			//fQuery.printTAG ( media_id );
			PhotoDetails newDetail = new PhotoDetails ( userID, media_id, fConnection, this );
			newDetail.setVisible ( true );

		} else {
			fObj.PrintDialog ( "Eccezione", "Selezionare un file" );
		}
    }//GEN-LAST:event_jButton4ActionPerformed

	/**
	 * filtra i contenuti della tabella con la ricerca del titolo
	 *
	 * @param evt
	 */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

		String txt = jTextField1.getText ();
		if ( txt.equals ( "" ) == true ) {
			execute ();
		} else {
			try {
				DBTableModel dataModel = fQuery.filterUserList ( userID, txt );
				if ( dataModel != null ) {
					jTable1.setModel ( dataModel );
					jTable1.removeColumn ( jTable1.getColumnModel ().getColumn ( 0 ) );
					//jPanel1.setVisible ( true );
					jScrollPane1.setVisible ( true );
					jTable1.setVisible ( true );
				}
			} catch ( SQLException ex ) {
				Logger.getLogger ( UserPhotosList.class.getName () ).log ( Level.SEVERE, null, ex );
				fObj.PrintDialog ( ex.getMessage () );
			} catch ( NullPointerException ex ) {
				//noConnection ();
			}
		}

    }//GEN-LAST:event_jButton1ActionPerformed
	/**
	 * cancella il file selezionato
	 *
	 * @param evt
	 */
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
		int sel_row = jTable1.getSelectedRow ();
		int media_id;
		String path;
		if ( sel_row != -1 ) {

			int res = JOptionPane.showConfirmDialog ( rootPane, "Cancellare il file selezionato?", null, JOptionPane.YES_NO_OPTION );

			if ( res == 0 ) {

				try {
					Statement stmt1 = null;
					ResultSet rset1 = fQuery.getMediaListIDFromUser ( userID );

					rset1.absolute ( sel_row + 1 );
					media_id = rset1.getInt ( 1 );
					path = rset1.getString ( "PATH" );
					/*delete da tabella*/
					fQuery.deleteMedia ( media_id );
					//delete da ftp
					UserInterfaceClass UIclass = new UserInterfaceClass ( fConnection );
					UIclass.ftpDeleteSingle ( fConnection.getFTPclient (), media_id, userID, path );
					//delete dei tag correlati
					fQuery.deleteTag ( media_id );
					execute ();
					fObj.PrintDialog ( "File cancellato" );
				} catch ( SQLException ex ) {
					fObj.PrintDialog ( ex.getMessage () );
					Logger.getLogger ( UserPhotosList.class.getName () ).log ( Level.SEVERE, null, ex );
				} catch ( SocketException ex ) {
					Logger.getLogger ( UserPhotosList.class.getName () ).log ( Level.SEVERE, null, ex );
				} finally {
					//System.out.println ( sel_row );
				}
			}
		} else {
			fObj.PrintDialog ( "Eccezione", "E' necessario selezionare un file" );
		}
    }//GEN-LAST:event_jButton2ActionPerformed
	/**
	 * apre l'url dell'immagine nel browser
	 *
	 * @param evt
	 */
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
		int sel_row = jTable1.getSelectedRow ();
		int media_id;
		String path;
		FlickrFunctions fobj = new FlickrFunctions ();
		if ( sel_row != -1 ) {
			try {
				ResultSet rset1 = fQuery.getMediaListIDFromUser ( userID );
				rset1.absolute ( sel_row + 1 );
				media_id = rset1.getInt ( 1 );
				path = rset1.getString ( "PATH" );

				if ( path.equals ( "NULL" ) == false ) {
					fobj.launchBrowser ( path );
				} else {
					fObj.PrintDialog ( "Impossibile aprire il file\n\nIl file non è stato caricato su ftp");
				}
			} catch ( SQLException ex ) {
				Logger.getLogger ( UserPhotosList.class.getName () ).log ( Level.SEVERE, null, ex );
			}
		} else {
			fObj.PrintDialog ( "Eccezione", "E' necessario selezionare un file" );
		}
    }//GEN-LAST:event_jButton3ActionPerformed

	/**
	 * finestra di caricamento foto
	 *
	 * @param evt
	 */
    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
		FileChoose2 chooser = new FileChoose2 ( fConnection, userID, getBar (), jButton6, this );
		chooser.setVisible ( true );

    }//GEN-LAST:event_jButton6ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
