/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr.user;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Eddy
 */
public class ImagesFilter extends FileFilter {

	/**
	 * Costruttore della classe.
	 */
	public ImagesFilter () {
		//costruttore vuoto
	}

	@Override
	/**
	 * Metodo che accetta un particolare tipo di file.
	 *
	 * @param file il file che viene esaminato per essere accettato
	 * @return vero o falso a seconda del file da accettare
	 */
	public boolean accept ( File file ) {
		/**
		 * Sono permesse soltanto cartelle e immagini
		 */
		return file.isDirectory () || file.getAbsolutePath ().toLowerCase ().endsWith ( ".bmp" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".gif" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".jpeg" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".jpg" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".png" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".wbmp" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".pcx" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".iff" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".ras" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".pbm" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".pgm" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".ppm" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".psd" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".swf" );
	}

	@Override
	/**
	 * Metodo che crea una stringa col tipo di file accettati e in particolare
	 * le estensioni che accetta.
	 *
	 * @return la stringa che esplicita i video accettati
	 */
	public String getDescription () {
		/**
		 * La descrizione verrà visualizzata nel menu a tendina
		 */
		return "Immagini (*.bmp;*.jpg;*.jpeg;*.wbmp;*.png;*.gif;*.pcx;*.iff;*.ras;*.pbm;*.pgm;*.ppm;*.psd;*.swf)";
	}
}
