/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flickr.user;

import flickr.DBTableModel;
import flickr.FQueries;
import flickr.FlickrConnection;
import flickr.FlickrFunctions;
import flickr.subpackage.test.TestQueryTable;
import java.math.BigDecimal;
import java.net.SocketException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ozeta
 */
public class UserSetlist extends ToolBar {

	FlickrFunctions fObj = new FlickrFunctions ();
	FQueries fQuery;
	Statement stmt = null;
	ResultSet rset = null;

	public UserSetlist ( FlickrConnection conn, int ID ) {
		super.fConnection = conn;
		this.fConnection = conn;
		fQuery = new FQueries ( conn );
		this.userID = ID;
		initComponents ();
		jTable1.setVisible ( false );
		setPanelTitle ( "Lista set" );
		execute ();
	}

	/**
	 * esegue la query sul db e riempie la tabella
	 */
	protected final void execute () {
		try {

			DBTableModel dataModel = fQuery.getSetListFromUser ( userID );

			if ( dataModel != null ) {

				jTable1.setModel ( dataModel );
				jTable1.removeColumn ( jTable1.getColumnModel ().getColumn ( 0 ) );

				jScrollPane1.setVisible ( true );
				jTable1.setVisible ( true );
			}

		} catch ( SQLException ex ) {
			Logger.getLogger ( UserSetlist.class.getName () ).log ( Level.SEVERE, null, ex );
			fObj.PrintDialog ( ex.getMessage () );
		} catch ( NullPointerException ex ) {
			//noConnection ();
		}
	}

	@SuppressWarnings ("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton4 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jButton4.setText("Visualizza Dettagli");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel1.setText("Filtra per Nome o descrizione");

        jTextField1.setText("titolo");

        jButton1.setText("Filtra");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cancella selezionato");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Nuovo set");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(215, 215, 215)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 262, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(348, 348, 348)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(373, 373, 373)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(361, 361, 361)
                                .addComponent(jButton3)
                                .addGap(20, 20, 20)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1)
                            .addComponent(jButton4))))
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap(131, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
/**
	 * visualizza la finestra "Dettagli e Modifica Set"
	 *
	 * @param evt
	 */
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
		int sel_row = jTable1.getSelectedRow ();
		int set_id;
		if ( sel_row != -1 ) {
			try {
				Statement stmt1 = null;
				set_id = fQuery.getSetIDFromUser ( userID, sel_row + 1 );

				System.out.println ( "set id= " + set_id );
				BigDecimal test = (BigDecimal) jTable1.getModel ().getValueAt ( jTable1.getSelectedRow (), 0 );
				String str = (String) jTable1.getModel ().getValueAt ( jTable1.getSelectedRow (), 1 );
				String str1 = (String) jTable1.getModel ().getValueAt ( jTable1.getSelectedRow (), 2 );
				set_id = Integer.valueOf ( test.intValue () );
				SetView newSet = new SetView ( fConnection, userID, set_id, str, str1, this );
				newSet.setVisible ( true );

			} catch ( SQLException ex ) {
				fObj.PrintDialog ( ex.getMessage () );
				Logger.getLogger ( UserSetlist.class.getName () ).log ( Level.SEVERE, null, ex );
			} finally {
			}
		} else {
			fObj.PrintDialog ( "Eccezione", "Selezionare un Set" );
		}
    }//GEN-LAST:event_jButton4ActionPerformed
	/**
	 * cancella set selezionato
	 *
	 * @param evt
	 */
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
		int sel_row = jTable1.getSelectedRow ();
		int set_id;
		if ( sel_row != -1 ) {
			try {
				Statement stmt1 = null;

				BigDecimal test = (BigDecimal) jTable1.getModel ().getValueAt ( jTable1.getSelectedRow (), 0 );
				set_id = Integer.valueOf ( test.intValue () );
				System.out.println ( "delete set id= " + set_id );
				fQuery.deleteSet ( set_id );
				execute ();
			} catch ( SQLException ex ) {
				fObj.PrintDialog ( ex.getMessage () );
				Logger.getLogger ( UserSetlist.class.getName () ).log ( Level.SEVERE, null, ex );
			} finally {
			}
		} else {
			fObj.PrintDialog ( "Eccezione", "Selezionare un Set" );
		}
    }//GEN-LAST:event_jButton2ActionPerformed
	/**
	 * nuovo set
	 *
	 * @param evt
	 */
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

		int set_id;

		try {
			String title = "Nuovo set";
			String desc = "";
			set_id = fQuery.newSet ( userID, title, desc );
			execute ();
			System.out.println ( "set id= " + set_id );

			SetView newSet = new SetView ( fConnection, userID, set_id, title, desc, this );
			newSet.setVisible ( true );

		} catch ( SQLException ex ) {
			fObj.PrintDialog ( ex.getMessage () );
			Logger.getLogger ( UserSetlist.class.getName () ).log ( Level.SEVERE, null, ex );
		} finally {
		}

    }//GEN-LAST:event_jButton3ActionPerformed

	/**
	 * filtra i contenuti della tabella con la ricerca del titolo
	 *
	 * @param evt
	 */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

		String txt = jTextField1.getText ();
		if ( txt.equals ( "" ) == true ) {
			execute ();
		} else {
			try {
				//esegue la query, compila il dataModel e mostra la tabella aggiornata
				DBTableModel dataModel = fQuery.filterSetList ( userID, txt );
				if ( dataModel != null ) {
					jTable1.setModel ( dataModel );
					jTable1.removeColumn ( jTable1.getColumnModel ().getColumn ( 0 ) );
					jScrollPane1.setVisible ( true );
					jTable1.setVisible ( true );
				}
			} catch ( SQLException ex ) {
				Logger.getLogger ( UserSetlist.class.getName () ).log ( Level.SEVERE, null, ex );
				fObj.PrintDialog ( ex.getMessage () );
			} catch ( NullPointerException ex ) {
				fObj.PrintDialog ( ex.getMessage () );
			}
		}

    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables
}
