/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flickr.user;

import flickr.DBTableModel;
import flickr.FQueries;
import flickr.FlickrConnection;
import flickr.FlickrFunctions;
import flickr.subpackage.NoRowTableSelectedException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Eddy
 */
public class UserPMList extends ToolBar {

	/**
	 * Attributi relativi alla connessione
	 */
	FlickrFunctions fObj = new FlickrFunctions ();
	FQueries fQuery;
	Statement stmt = null;
	ResultSet rset = null;

	/**
	 * Attributi relativi alla query
	 */
	String sel = "SELECT P.MESSAGE_ID as ID, U1.SCREEN_NAME as MITTENTE, U2.SCREEN_NAME as DESTINATARIO,"
			+ " SUBJECT as OGGETTO, TEXT as TESTO, REPLACE(REPLACE(READ_FLAG,0,'Non letto'), 1,'Letto') as LETTO, TO_CHAR (PMSG_DATE, 'DD-MON-YYYY (HH24:MI)') as DATA";
	String fr;
	String where;
	String order = " ORDER BY DATA DESC";

	/**
	 * Attributi relativi al pm selezionato
	 */
	BigDecimal pmId;
	String username;
	String recipient;
	String subject;
	String text;
	DBTableModel dataModel;

	/**
	 * Creates new form UserPhotosList
	 */
	/**
	 * Costruttore che setta il valore di fConnection identico a quello passato
	 * in input, stessa cosa vale per userID, inizializza le componenti, setta
	 * la clausola from e where della query con il significato dei valori
	 * predefiniti delle combobox, mostra la tabella, setta il titolo e recupera
	 * l'username dell'utente (utile per future query).
	 *
	 * @param conn di tipo FlickrConnection
	 * @param ID di tipo int
	 */
	public UserPMList ( FlickrConnection conn, int ID ) {
		super.fConnection = conn;
		this.fConnection = conn;
		fQuery = new FQueries ( conn );
		this.userID = ID;
		initComponents ();
		jTable1.setVisible ( false );
		fr = " FROM " + fQuery.getSchema () + ".USERS U1"
				+ " JOIN " + fQuery.getSchema () + ".PERSONAL_MESSAGE P"
				+ " ON U1.USER_ID = P.USER_FROM"
				+ " JOIN " + fQuery.getSchema () + ".USERS U2"
				+ " ON P.USER_TO = U2.USER_ID";
		where = " WHERE U1.USER_ID = " + userID
				+ " OR U2.USER_ID = " + userID;
		execute ();
		super.setTitle ( "Lista PM" );
		recoverUsername ();
	}

	/**
	 * Metodo che mostra la tabella dopo aver eseguito la query
	 */
	protected final void execute () {
		try {

			dataModel = fQuery.getPMList ( sel + fr + where + order );

			if ( dataModel != null ) {
				jTable1.setModel ( dataModel );
				jTable1.removeColumn ( jTable1.getColumnModel ().getColumn ( 0 ) );
				jScrollPane1.setVisible ( true );

				jTable1.setVisible ( true );
			}

		} catch ( SQLException ex ) {
			Logger.getLogger ( UserPMList.class.getName () ).log ( Level.SEVERE, null, ex );
			fObj.PrintDialog ( ex.getMessage () );
		} catch ( NullPointerException ex ) {
			//noConnection ();
		}
	}

	@SuppressWarnings ("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jButton1.setText("Nuovo Messaggio");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Apri Messaggio");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Cancella Messaggio");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Rispondi");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Segna come letto");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tutti", "Ricevuti", "Inviati" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Tipi di PM:");

        jLabel2.setText("Ordina per:");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Più Recenti", "Meno Recenti" }));
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(160, 160, 160)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox2, 0, 130, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 110, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(110, 110, 110)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(48, 48, 48))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(328, 328, 328)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton3)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(jButton5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton4)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(166, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

	/**
	 * Metodo che, se premuto il pulsante "Apri Messaggio" e selezionatone uno,
	 * lo visualizza in un'altra finestra.
	 *
	 * @param evt di tipo java.awt.event.ActionEvent
	 */
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
		// TODO add your handling code here:
		String sender = getFromName ();
		String recipient = getToName ();
		String subject = getPMSubject ();
		text = getPMText ();
		ViewPM vpm = new ViewPM ( sender, recipient, subject, text );
		vpm.setVisible ( true );
		jButton5ActionPerformed ( null );
    }//GEN-LAST:event_jButton2ActionPerformed

	/**
	 * Metodo che, se premuto il pulsante "Cancella Messaggio" e selezionatone
	 * uno, lo cancella.
	 *
	 * @param evt di tipo java.awt.event.ActionEvent
	 */
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
		// TODO add your handling code here:
		try {
			int res = JOptionPane.showConfirmDialog ( rootPane, "Vuoi cancellare il messaggio?", null, JOptionPane.YES_NO_OPTION );

			if ( res == 0 ) {
				pmId = getMessageId ();
				int pm_id = pmId.intValue ();
				fQuery.deleteFromPM ( pm_id );
				execute ();
			}
		} catch ( NoRowTableSelectedException ex ) {
			javax.swing.JOptionPane.showMessageDialog ( null,
					"Non è stato selezionato alcun messaggio.",
					"Errore cancellazione",
					javax.swing.JOptionPane.ERROR_MESSAGE );
		} catch ( NullPointerException ex ) {
			javax.swing.JOptionPane.showMessageDialog ( null,
					"È stata selezionata una riga vuota.",
					"Errore cancellazione",
					javax.swing.JOptionPane.ERROR_MESSAGE );
		} catch ( SQLException ex ) {
			javax.swing.JOptionPane.showMessageDialog ( null,
					"Non è stato possibile cancellare il messaggio.",
					"Problema cancellazione",
					javax.swing.JOptionPane.ERROR_MESSAGE );
		}
    }//GEN-LAST:event_jButton3ActionPerformed

	/**
	 * Metodo che, se premuto il pulsante "Rispondi" e selezionatone uno, apre
	 * una finestra per poter rispondere a quel messaggio (se e solo se non si è
	 * il mittente di quel messaggio).
	 *
	 * @param evt di tipo java.awt.event.ActionEvent
	 */
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
		// TODO add your handling code here:
		recipient = getFromName ();
		subject = getPMSubject ();
		if ( username == null ) {
			recoverUsername ();
		}
		if ( username.equals ( recipient ) ) {
			javax.swing.JOptionPane.showMessageDialog ( null,
					"Non è possibile rispondere a un messaggio inviato.",
					"Problema risposta",
					javax.swing.JOptionPane.ERROR_MESSAGE );
		} else {
			NewPM reply = new NewPM ( fConnection, userID, recipient, subject );
			jButton5ActionPerformed ( null );
			reply.setVisible ( true );
		}
		execute ();
    }//GEN-LAST:event_jButton4ActionPerformed

	/**
	 * Metodo che, se premuto il pulsante "Nuovo Messaggio", apre la finestra
	 * con la possibilità di inviare un messaggio a un utente diverso da se
	 * stesso.
	 *
	 * @param evt di tipo java.awt.event.ActionEvent
	 */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
		// TODO add your handling code here:
		NewPM nPM = new NewPM ( fConnection, username );
		nPM.setVisible ( true );
		execute ();
    }//GEN-LAST:event_jButton1ActionPerformed

	/**
	 * Metodo che, se premuto il pulsante "Segna come letto" e selezionatone
	 * uno, lo segna come letto. (Ovviamente non è possibile segnare come letto
	 * i messaggi inviati)
	 *
	 * @param evt di tipo java.awt.event.ActionEvent
	 */
    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
		// TODO add your handling code here:
		if ( !username.equals ( getFromName () ) ) {
			try {
				fQuery.setReadPM ( getMessageId ().intValue () );
				execute ();
			} catch ( SQLException ex ) {
				Logger.getLogger ( UserPMList.class.getName () ).log ( Level.SEVERE, null, ex );
			}
		}
    }//GEN-LAST:event_jButton5ActionPerformed

	/**
	 * Metodo che, se premuto il combobox dei tipi di PM e selezionato il tipo
	 * prescelto, mostra i PM di quel tipo oppure tutti.
	 *
	 * @param evt di tipo java.awt.event.ActionEvent
	 */
    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
		// TODO add your handling code here:
		if ( jComboBox2.getSelectedItem ().toString ().equals ( "Più recenti" ) ) {
			order = " ORDER BY DATA DESC";
		} else {
			order = " ORDER BY DATA";
		}
		execute ();
    }//GEN-LAST:event_jComboBox2ActionPerformed

	/**
	 * Metodo che ordina i PM in ordine cronologico (o i più recenti o i più
	 * datati).
	 *
	 * @param evt di tipo java.awt.event.ActionEvent
	 */
    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
		// TODO add your handling code here:
		if ( jComboBox1.getSelectedItem ().toString ().equals ( "Tutti" ) ) {
			where = " WHERE U1.USER_ID = " + userID
					+ " OR U2.USER_ID = " + userID;
		} else if ( jComboBox1.getSelectedItem ().toString ().equals ( "Ricevuti" ) ) {
			where = " WHERE U2.USER_ID = " + userID;
		} else {
			where = " WHERE U1.USER_ID = " + userID;
		}
		execute ();
    }//GEN-LAST:event_jComboBox1ActionPerformed

	/**
	 * Metodo che restituisce l'id del messaggio personale selezionato nella
	 * tabella. Può sollevare eccezioni.
	 *
	 * @return di tipo BigDecimal
	 */
	private BigDecimal getMessageId () {
		int row = jTable1.getSelectedRow ();
		if ( row == -1 ) {
			throw new NoRowTableSelectedException ();
		}
		BigDecimal test = (BigDecimal) jTable1.getModel ().getValueAt ( jTable1.getSelectedRow (), 0 );
		return test;
	}

	/**
	 * Metodo che restituisce l'username del mittente.
	 *
	 * @return di tipo String
	 */
	private String getFromName () {
		int row = jTable1.getSelectedRow ();
		if ( row == -1 ) {
			throw new NoRowTableSelectedException ();
		}
		String test = (String) jTable1.getModel ().getValueAt ( jTable1.getSelectedRow (), 1 );
		return test;
	}

	/**
	 * Metodo che restituisce l'username del destinatario.
	 *
	 * @return di tipo String
	 */
	private String getToName () {
		int row = jTable1.getSelectedRow ();
		if ( row == -1 ) {
			throw new NoRowTableSelectedException ();
		}
		String test = (String) jTable1.getModel ().getValueAt ( jTable1.getSelectedRow (), 2 );
		return test;
	}

	/**
	 * Metodo che restituisce l'oggetto del PM.
	 *
	 * @return di tipo String
	 */
	private String getPMSubject () {
		int row = jTable1.getSelectedRow ();
		if ( row == -1 ) {
			throw new NoRowTableSelectedException ();
		}
		String test = (String) jTable1.getModel ().getValueAt ( jTable1.getSelectedRow (), 3 );
		return test;
	}

	/**
	 * Metodo che restituisce il corpo del testo del PM.
	 *
	 * @return di tipo String
	 */
	private String getPMText () {
		int row = jTable1.getSelectedRow ();
		if ( row == -1 ) {
			throw new NoRowTableSelectedException ();
		}

		String test = (String) jTable1.getModel ().getValueAt ( jTable1.getSelectedRow (), 4 );
		return test;
	}

	/**
	 * Metodo che imposta il nome dell'utente che sta eseguendo l'applicazione
	 */
	private void recoverUsername () {
		try {
			username = fQuery.getUsernameFromId ( userID );
		} catch ( SQLException ex ) {
			System.out.println ( "Problema nel recupero username.\n" );
		}
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
