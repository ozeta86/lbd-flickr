/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr.user;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Eddy
 */
public class VideosFilter extends FileFilter {

	/**
	 * Costruttore della classe.
	 */
	public VideosFilter () {
		//costruttore vuoto
	}

	@Override
	/**
	 * Metodo che accetta un particolare tipo di file.
	 *
	 * @param file il file che viene esaminato per essere accettato
	 * @return vero o falso a seconda del file da accettare
	 */
	public boolean accept ( File file ) {
		/**
		 * Sono permesse soltanto cartelle e video
		 */
		return file.isDirectory () || file.getAbsolutePath ().toLowerCase ().endsWith ( ".mkv" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".webm" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".flv" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".ogg" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".avi" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".mov" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".qt" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".wmv" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".mp4" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".mpeg" )
				|| file.getAbsolutePath ().toLowerCase ().endsWith ( ".3gp" );
	}

	@Override
	/**
	 * Metodo che crea una stringa col tipo di file accettati e in particolare
	 * le estensioni che accetta.
	 *
	 * @return la stringa che esplicita i video accettati
	 */
	public String getDescription () {
		/**
		 * La descrizione verrà visualizzata nel menu a tendina
		 */
		return "Video (*.mkv;*.webm;*.flv;*.ogg;*.avi;*.mov;*.qt;*.wmv;*.mp4;*.mpeg;*.3gp)";
	}
}
