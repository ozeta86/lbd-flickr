/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Oz
 */
public class FlickrFunctions {

	private String title = "Avviso";

	/**
	 * Dialog box di avviso dove vengono mostrate eccezioni, avvisi ed errori
	 * all'utente
	 *
	 * @param message
	 */
	final public void PrintDialog ( String message ) {

		JOptionPane.showMessageDialog ( null, message, title, JOptionPane.INFORMATION_MESSAGE );

	}

	final public void PrintAbout (  ) {
		String title = "Credits";
		String message = "Progetto Laboratorio Basi Dati\n"
				+ "Docente: Alessandro De Luca\n\n"
				+ "Flickr\n\n"
				+ "Marco Carrozzo - N86-1240\n\n" + "Eddy Pasquale Marino - N86-1133";
		JOptionPane.showMessageDialog ( null, message, title, JOptionPane.INFORMATION_MESSAGE );

	}
	final public void PrintDialog ( String title, String message ) {

		JOptionPane.showMessageDialog ( null, message, title, JOptionPane.INFORMATION_MESSAGE );

	}

	final public void PrintDialog ( String title, String message, int type ) {

		JOptionPane.showMessageDialog ( null, message, title, type );

	}

	final public boolean check ( Object obj, String message ) {
		if ( obj == null ) {
			System.out.println ( "unable to read memory: " + message );
			return false;
		} else {
			return true;
		}
	}

	/**
	 * procedura che apre il pdf di guida
	 *
	 * @param path
	 */
	final public void displayGuide ( String path ) {
		if ( Desktop.isDesktopSupported () ) {
			try {

				File myFile = new File ( (path) );
				Desktop.getDesktop ().open ( myFile );
			} catch ( IOException ex ) {
				this.PrintDialog ( "Eccezione", "Impossibile aprire il file" );
				System.out.println ( ex.getMessage () );
			} catch ( IllegalArgumentException ex ) {
				this.PrintDialog ( "Eccezione", "il file non esiste" );
				System.out.println ( ex.getMessage () );
			} catch ( UnsupportedOperationException ex ) {
				this.PrintDialog ( "Eccezione", "Impossibile aprire il file" );
				System.out.println ( ex.getMessage () );
			}
		}

	}

	/**
	 * apre una pagina web nel browser
	 *
	 * @param path
	 */
	final public void launchBrowser ( String path ) {

		path = "http://" + path;
		path = path.replace ( " ", "%20" );
		try {
			URL url = new URL ( path );
			Desktop desktop = Desktop.isDesktopSupported () ? Desktop.getDesktop () : null;
			URI uri = url.toURI ();
			if ( desktop != null && desktop.isSupported ( Desktop.Action.BROWSE ) ) {
				try {
					desktop.browse ( uri );
				} catch ( Exception e ) {
					e.printStackTrace ();
				}
			}
		} catch ( MalformedURLException ex ) {
			Logger.getLogger ( FlickrFunctions.class.getName () ).log ( Level.SEVERE, null, ex );
		} catch ( URISyntaxException e ) {
			e.printStackTrace ();
		}
	}

}
