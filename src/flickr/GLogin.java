/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr;

import java.awt.Component;
import java.awt.Cursor;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 * GUI LOGIN
 *
 * @author Oz
 */
public class GLogin extends javax.swing.JFrame {

	/**
	 * thread background di supporto. avvia la connessione al database in base
	 * ai parametri impostati nei campi
	 */
	private class ConnectionThread extends Thread {

		private int test;

		public ConnectionThread ( int test ) {
			this.test = test;
		}

		public void testConnection () {
			start ();
		}

		/**
		 * avvia la connessione su localhost o indirizzo esterno
		 *
		 * @throws NumberFormatException
		 * @throws SQLException
		 */
		private void connectToServer () throws NumberFormatException, SQLException {
			if ( localhost.isSelected () ) {
				connection = new FlickrConnection ( url, porta, service, utente, password, schema );
			} else {
				connection = new FlickrConnection ();
			}
		}

		public void run () {
			Home mainPanel;

			try {
				System.out.println ( "connessione al db..." );
				progressBar1.setVisible ( true );
				progressBar1.setIndeterminate ( true );
				progressBar1.newThread ();
				setCursor ( Cursor.getPredefinedCursor ( Cursor.WAIT_CURSOR ) );
				/**
				 * esegue la routine appropriata in base ai parametri impostati
				 */
				switch ( test ) {
					case TESTCONNECTION: //test della connessione
						System.out.println ( porta + " " + url );
						connection = new FlickrConnection ( url, porta, service, utente, password, schema );
						progressBar1.stopProgress ();
						fObj.PrintDialog ( "Test connessione", "Connessione stabilita" );
						connection.closeConnection ();
						break;
					case ADMINCONNECTION: //connessione pannello admin
						connectToServer ();
						mainPanel = new Home ( connection );
						mainPanel.setVisible ( true );
						dispose ();
						break;
					case USERCONNECTION: //connessione pannello utente
						connectToServer ();
						flickr.user.UserLogin userLogin = new flickr.user.UserLogin ( connection );
						userLogin.setVisible ( true );
						dispose ();
						break;
					case USERREGISTRATION: //registrazione utente
						connectToServer ();
						flickr.user.Registration userWindow = new flickr.user.Registration ( connection );
						userWindow.setVisible ( true );
						dispose ();
						break;
				}

			} catch ( NumberFormatException ex ) {
				fObj.PrintDialog ( "Errore", "Porta non valida\n" + ex.getMessage (), JOptionPane.ERROR_MESSAGE );
			} catch ( SQLException ex ) {
				fObj.PrintDialog ( "Errore", ex.getMessage (), JOptionPane.ERROR_MESSAGE );
			} finally {
				setCursor ( Cursor.getPredefinedCursor ( Cursor.DEFAULT_CURSOR ) );
				progressBar1.stopProgress ();
				PREMUTO = 0;
			}
		}
	}

	//libreria funzioni di appoggio
	FlickrFunctions fObj = new FlickrFunctions ();
	//libreria funzioni di connessione
	FlickrConnection connection;
	String tmp;
	String url;
	String service;
	String utente;
	String password;
	String schema;
	String porta;

	protected final int DEFAULTCONNECTION = 0;
	protected final int TESTCONNECTION = 1;
	protected final int ADMINCONNECTION = 2;
	protected final int USERCONNECTION = 3;
	protected final int USERREGISTRATION = 4;
	protected int PREMUTO = 0;

	/**
	 * Creates new form GLogin
	 */
	private void setButtons ( Boolean var ) {
		jtUtente.setEnabled ( var );
		jPasswordField1.setEnabled ( var );
		jtPort.setEnabled ( var );
		jtHostname.setEnabled ( var );
		jtService.setEnabled ( var );
		jtSchema.setEnabled ( var );
		testButton.setEnabled ( var );
	}

	public GLogin () {
		setAesthetics ();
		//inizializzazione dei componenti
		initComponents ();

		//creazione e configurazione buttongroup Login
		ButtonGroup bG = new ButtonGroup ();
		bG.add ( localhost );
		bG.add ( remote );
		//imposto un radiobutton selezionato
		remote.setSelected ( true );
		setFields ( "gruppo15", "Pass01_Pass01", "1521", "143.225.117.238", "xe", "GRUPPO15" );
		progressBar1.setVisible ( false );

	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings ("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        remote = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        localhost = new javax.swing.JRadioButton();
        jtUtente = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jtPort = new javax.swing.JTextField();
        testButton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jtHostname = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jtService = new javax.swing.JTextField();
        jPasswordField1 = new javax.swing.JPasswordField();
        jLabel8 = new javax.swing.JLabel();
        jtSchema = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        annullaButton = new javax.swing.JButton();
        progressBar1 = new flickr.subpackage.ProgressBar();
        jPanel3 = new javax.swing.JPanel();
        adminButton = new javax.swing.JButton();
        registrationButton = new javax.swing.JButton();
        userButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Flickr: Login");
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        remote.setText("Server Unina ( Gruppo 15 )");
        remote.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remoteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(remote)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(remote)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(jPanel1.getBorder());

        localhost.setText("Server custom");
        localhost.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                localhostActionPerformed(evt);
            }
        });

        jtUtente.setText("gruppo15");
        jtUtente.setEnabled(false);
        jtUtente.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtUtenteFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtUtenteFocusLost(evt);
            }
        });
        jtUtente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtUtenteActionPerformed(evt);
            }
        });

        jLabel1.setText("Utente");

        jLabel2.setText("Password");

        jLabel3.setText("Porta");

        jtPort.setText("1521");
        jtPort.setEnabled(false);
        jtPort.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtPortFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtPortFocusLost(evt);
            }
        });

        testButton.setText("Test");
        testButton.setEnabled(false);
        testButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testButtonActionPerformed(evt);
            }
        });

        jLabel5.setText("Host");

        jtHostname.setText("143.225.117.238");
        jtHostname.setEnabled(false);
        jtHostname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtHostnameFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtHostnameFocusLost(evt);
            }
        });

        jLabel6.setText("Servizio");

        jtService.setText("xe");
        jtService.setEnabled(false);
        jtService.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtServiceFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtServiceFocusLost(evt);
            }
        });

        jPasswordField1.setText("Pass01_Pass01");
        jPasswordField1.setEnabled(false);
        jPasswordField1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jPasswordField1FocusGained(evt);
            }
        });

        jLabel8.setText("Schema");

        jtSchema.setText("flickr");
        jtSchema.setEnabled(false);
        jtSchema.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jtSchemaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jtSchemaFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5))
                .addGap(27, 27, 27)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jtPort)
                    .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                    .addComponent(jtUtente)
                    .addComponent(jtHostname, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jtService)
                    .addComponent(jtSchema, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE))
                .addGap(35, 35, 35))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(localhost)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(testButton)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(localhost)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtHostname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jtService, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtUtente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel8)
                    .addComponent(jtSchema, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(testButton)
                .addGap(16, 16, 16))
        );

        jLabel4.setText("oppure");

        jLabel7.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Flickr Manager: Login");
        jLabel7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        annullaButton.setText("Esci");
        annullaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annullaButtonActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        adminButton.setText("Connetti Admin");
        adminButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adminButtonActionPerformed(evt);
            }
        });

        registrationButton.setText("Registra Nuovo Utente");
        registrationButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrationButtonActionPerformed(evt);
            }
        });

        userButton.setText("Connetti Utente");
        userButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(userButton, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(adminButton, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(registrationButton, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(adminButton)
                    .addComponent(userButton))
                .addGap(18, 18, 18)
                .addComponent(registrationButton)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(226, Short.MAX_VALUE)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(227, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(278, 278, 278))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(progressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(annullaButton))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(annullaButton)
                        .addContainerGap())
                    .addComponent(progressBar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void localhostActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_localhostActionPerformed
		//configurazione alla pressione del radio "localhost "
		setButtons ( true );
    }//GEN-LAST:event_localhostActionPerformed

    private void jtUtenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtUtenteActionPerformed

    }//GEN-LAST:event_jtUtenteActionPerformed
	private void choiseConnection ( int choise ) {
		if ( PREMUTO == 0 ) {

			PREMUTO = 1;
			if ( localhost.isSelected () ) {
				password = new String ( jPasswordField1.getPassword () );
				porta = jtPort.getText ();
				schema = jtSchema.getText ();
				service = jtService.getText ();
				url = jtHostname.getText ();
				utente = jtUtente.getText ();
			}
			ConnectionThread cThread = new ConnectionThread ( choise );
			new Thread ( cThread ).start ();
		}
	}

	private void setFields ( String utente, String password, String port, String host, String service, String schema ) {
		jtUtente.setText ( utente );
		jPasswordField1.setText ( password );
		jtPort.setText ( port );
		jtHostname.setText ( host );
		jtService.setText ( service );
		jtSchema.setText ( schema );

	}

	private void getField ( JTextField field ) {
		tmp = field.getText ();
		field.setText ( "" );
	}

	private void resetField ( JTextField field ) {
		if ( field.getText ().equals ( "" ) ) {
			field.setText ( tmp );
		}
	}
    private void remoteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remoteActionPerformed
		//configurazione alla pressione del radio "remote "
		setButtons ( false );

    }//GEN-LAST:event_remoteActionPerformed

    private void adminButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adminButtonActionPerformed
		choiseConnection ( ADMINCONNECTION );
    }//GEN-LAST:event_adminButtonActionPerformed

    private void jtUtenteFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtUtenteFocusGained
		getField ( jtUtente );
    }//GEN-LAST:event_jtUtenteFocusGained

    private void jtPortFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtPortFocusGained
		getField ( jtPort );
    }//GEN-LAST:event_jtPortFocusGained

    private void testButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_testButtonActionPerformed
		//tasto Test: testa la connessione con i parametri inseriti
		choiseConnection ( TESTCONNECTION );
    }//GEN-LAST:event_testButtonActionPerformed

    private void jtHostnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtHostnameFocusGained
		getField ( jtHostname );
    }//GEN-LAST:event_jtHostnameFocusGained

    private void jtServiceFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtServiceFocusGained
		getField ( jtService );
    }//GEN-LAST:event_jtServiceFocusGained

    private void jtUtenteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtUtenteFocusLost
		resetField ( jtUtente );
    }//GEN-LAST:event_jtUtenteFocusLost

    private void annullaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_annullaButtonActionPerformed
		System.exit ( 0 );
    }//GEN-LAST:event_annullaButtonActionPerformed

    private void jtHostnameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtHostnameFocusLost
		resetField ( jtHostname );
    }//GEN-LAST:event_jtHostnameFocusLost

    private void jPasswordField1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jPasswordField1FocusGained
		getField ( jPasswordField1 );
    }//GEN-LAST:event_jPasswordField1FocusGained

    private void jtPortFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtPortFocusLost
		resetField ( jtPort );
    }//GEN-LAST:event_jtPortFocusLost

    private void jtServiceFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtServiceFocusLost
		resetField ( jtService );
    }//GEN-LAST:event_jtServiceFocusLost

    private void jtSchemaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtSchemaFocusGained
		getField ( jtSchema );
    }//GEN-LAST:event_jtSchemaFocusGained

    private void jtSchemaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jtSchemaFocusLost
		resetField ( jtSchema );
    }//GEN-LAST:event_jtSchemaFocusLost

    private void registrationButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrationButtonActionPerformed
		//registrazione utente
		choiseConnection ( USERREGISTRATION );
    }//GEN-LAST:event_registrationButtonActionPerformed

    private void userButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userButtonActionPerformed
		//connessione utente
		choiseConnection ( USERCONNECTION );
    }//GEN-LAST:event_userButtonActionPerformed

	private void setAesthetics () {
		javax.swing.ImageIcon img = new javax.swing.ImageIcon ( getClass ().getResource ( "/flickr/res/images/flickr_logo.png" ) );
		setIconImage ( img.getImage () );
		try {
			UIManager.setLookAndFeel (
					UIManager.getSystemLookAndFeelClassName () );
		} catch ( ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex ) {
			Logger.getLogger ( GLogin.class.getName () ).log ( Level.SEVERE, null, ex );
		}
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton adminButton;
    private javax.swing.JButton annullaButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JTextField jtHostname;
    private javax.swing.JTextField jtPort;
    private javax.swing.JTextField jtSchema;
    private javax.swing.JTextField jtService;
    private javax.swing.JTextField jtUtente;
    private javax.swing.JRadioButton localhost;
    private flickr.subpackage.ProgressBar progressBar1;
    private javax.swing.JButton registrationButton;
    private javax.swing.JRadioButton remote;
    private javax.swing.JButton testButton;
    private javax.swing.JButton userButton;
    // End of variables declaration//GEN-END:variables
}
