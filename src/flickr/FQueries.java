/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr;

import flickr.subpackage.StringMatch;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oz
 */
public class FQueries {

	private String schema;
	private Connection connection;
	private FlickrConnection fConnection;
	private ResultSet rset = null;

	String selectMediaList = "SELECT M.MEDIA_ID, M.TITLE AS TITOLO, M.CAPTION AS DESCRIZIONE, M.NUMBER_VIEWS AS VISUALIZZAZIONI, M.STARS AS STELLE, M.PRIVACY AS PRIVACY, M.SAFE_RATE AS SAFE_RATE, M.COPYRIGHT AS COPYRIGHT  ";

	public FQueries ( FlickrConnection fConnection ) {
		this.fConnection = fConnection;
		this.schema = fConnection.getSchema ();
		this.connection = fConnection.getConnection ();
	}

	public String getSchema () {
		return schema;
	}

	/**
	 * restituisce il numero di oggetti contati
	 *
	 * @param query
	 * @return
	 * @throws java.sql.SQLException
	 */
	public int getCount ( String query ) throws SQLException {
		int count;
		Statement stmt;
		stmt = connection.createStatement ();
		rset = stmt.executeQuery ( query );
		rset.next ();
		count = rset.getInt ( 1 );

		stmt.close ();
		rset.close ();
		return count;
	}

	public Object getObject ( String query ) throws SQLException {

		return null;
	}

	/**
	 * scorre il resultset e stampa la prima colonna
	 *
	 * @param rs
	 * @throws SQLException
	 */
	private void printRs ( ResultSet rs ) throws SQLException {

		while ( rs.next () ) {
			System.out.println ( "media id: " + rs.getInt ( 1 ) );
		}
	}

	public void createUser ( String email, String password, String username, String name, String surname ) throws SQLException {
		String query = "INSERT INTO " + schema + ".USERS ( EMAIL, PASSWORD, SCREEN_NAME, NAME, SURNAME, REGISTERED )";
		query = query + " VALUES ( ?, ?, ?, ?, ?, SYSDATE )";
		String hashed = BCrypt.hashpw ( password, BCrypt.gensalt () );
		PreparedStatement stmt = connection.prepareStatement ( query );
		stmt.setString ( 1, email );
		stmt.setString ( 2, hashed );
		stmt.setString ( 3, username );
		stmt.setString ( 4, name );
		stmt.setString ( 5, surname );
		stmt.executeUpdate ();
		stmt.close ();
	}

	public void resetPassword ( String email, String password ) throws SQLException {
		String hashed = BCrypt.hashpw ( password, BCrypt.gensalt () );
		String query = " UPDATE " + schema + ".USERS "
				+ " SET PASSWORD = ? "
				+ " WHERE EMAIL = ? ";
		System.out.println ( query );
		PreparedStatement stmt = connection.prepareStatement ( query );
		stmt.setString ( 1, hashed );
		stmt.setString ( 2, email );
		stmt.executeUpdate ();
		stmt.close ();

	}

	public void insertMedia ( int id, String path, String title, String copyright, String safety, String privacy, String v_flag, double dim, int height, int width ) throws SQLException {
		String query = "INSERT INTO " + schema + ".MEDIA ( USER_ID, PATH, TITLE, COPYRIGHT, SAFE_RATE, PRIVACY, VIDEO_FLAG, DIMENSION, H_RESOLUTION, V_RESOLUTION, UPLOAD_TIME, ALLOW_COMMENT )";
		query = query + " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ? )";
		PreparedStatement stmt = connection.prepareStatement ( query );
		stmt.setInt ( 1, id );
		stmt.setString ( 2, path );
		stmt.setString ( 3, title );
		stmt.setString ( 4, copyright );
		stmt.setString ( 5, safety );
		stmt.setString ( 6, privacy );
		stmt.setString ( 7, v_flag );
		stmt.setDouble ( 8, dim );
		stmt.setInt ( 9, height );
		stmt.setInt ( 10, width );
		stmt.setInt ( 11, 1 );
		stmt.executeUpdate ();
		stmt.close ();
	}

	public void insertTag ( int MediaId, String tagText ) throws SQLException {
		String query = "INSERT INTO " + schema + ".TAG ( MEDIA_ID, TAG_TEXT )";
		query = query + " VALUES ( ?, ? )";
		PreparedStatement stmt = connection.prepareStatement ( query );
		stmt.setInt ( 1, MediaId );
		stmt.setString ( 2, tagText );
		stmt.executeUpdate ();
		stmt.close ();
	}

	public DBTableModel getUserInfo ( int userID ) throws SQLException {
		String query = "SELECT * FROM " + schema + ".USERS U WHERE U.USER_ID = " + userID;
		Statement stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rset = stmt.executeQuery ( query );
		DBTableModel dataModel = new DBTableModel ( rset, true );
		//System.out.println ( "colonne: " + dataModel.getColumnCount ());
		return dataModel;
	}

	public void updateUserInfo ( int userID, String username, String email, String name, String surname ) throws SQLException {

		String query = "UPDATE "
				+ schema + ".USERS U "
				+ " SET SCREEN_NAME = ?,  EMAIL = ?, NAME = ?, SURNAME = ? "
				+ " WHERE " + " U.USER_ID = ?";
		PreparedStatement stmt = connection.prepareStatement ( query, ResultSet.TYPE_SCROLL_INSENSITIVE );
		//System.out.println ( query );
		stmt.setString ( 1, username );
		stmt.setString ( 2, email );
		stmt.setString ( 3, name );
		stmt.setString ( 4, surname );
		stmt.setInt ( 5, userID );

		stmt.executeUpdate ();
		stmt.close ();

	}

	/**
	 * controlla l'associazione tra username e plainPassword
	 */
	public int getUserID ( String email, String plainPassword ) throws SQLException {
		int ID;
		String hashedPassword;
		String query = "SELECT USER_ID, PASSWORD FROM " + schema + ".USERS WHERE ( EMAIL = ? )";
		PreparedStatement stmt = connection.prepareStatement ( query );
		stmt.setString ( 1, email );
		ResultSet rs = stmt.executeQuery ();
		rs.next ();
		ID = rs.getInt ( 1 );
		hashedPassword = rs.getString ( 2 );
		if ( BCrypt.checkpw ( plainPassword, hashedPassword ) == false ) {
			SQLException ex = new SQLException ( "hash non valido" );
			throw ex;
		}

		System.out.println ( "combinazione valida. ID->" + ID );
		stmt.close ();
		rs.close ();

		return ID;
	}

	public int getIDFromEmail ( String email ) throws SQLException {
		int ID;
		String query = "SELECT USER_ID FROM " + schema + ".USERS WHERE EMAIL = ? ";

		PreparedStatement stmt = connection.prepareStatement ( query );
		stmt.setString ( 1, email );
		ResultSet rs = stmt.executeQuery ();
		rs.next ();
		ID = rs.getInt ( 1 );
		System.out.println ( "id: " + ID );
		stmt.close ();
		rs.close ();

		return ID;
	}

	public int getMediaIdFromPathAndID ( String path, int id ) throws SQLException {
		int MediaId;
		String query = "SELECT MEDIA_ID FROM " + schema + ".MEDIA WHERE PATH = ? AND USER_ID = ? ";
		PreparedStatement stmt = connection.prepareStatement ( query );
		stmt.setString ( 1, path );
		stmt.setInt ( 2, id );
		ResultSet rs = stmt.executeQuery ();
		rs.next ();
		MediaId = rs.getInt ( 1 );
		System.out.println ( "MediaId: " + MediaId );
		stmt.close ();
		rs.close ();
		return MediaId;
	}

	public DBTableModel getMediaListFromUser ( int id ) throws SQLException, NullPointerException {
		Statement stmt;
		String query1
				= selectMediaList
				//+ ", COUNT ( M.MEDIA_ID ) "
				+ " FROM "
				+ schema + ".MEDIA M "
				+ " JOIN "
				+ schema + ".USERS U ON  U.USER_ID = M.USER_ID "
				//+ " JOIN "
				//+ schema + ".TAG T ON  T.MEDIA_ID = M.MEDIA_ID "
				+ " WHERE U.USER_ID =" + id //+ "GROUP BY M.MEDIA_ID"
				+ " ORDER BY M.MEDIA_ID ";
		;
		stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rset = stmt.executeQuery ( query1 );
		DBTableModel dataModel = new DBTableModel ( rset, true );
		return dataModel;

	}

	public ResultSet getMediaListIDFromUser ( int id ) throws SQLException, NullPointerException {
		Statement stmt;
		String query1
				= "SELECT M.MEDIA_ID, M.PATH "
				+ " FROM "
				+ schema + ".MEDIA M JOIN "
				+ schema + ".USERS U ON  U.USER_ID = M.USER_ID "
				+ " WHERE U.USER_ID =" + id
				+ " ORDER BY M.MEDIA_ID ";
		stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rset = stmt.executeQuery ( query1 );
		DBTableModel dataModel = new DBTableModel ( rset, true );
		return rset;

	}

	public int getSetIDFromUser ( int userID, int sel_row ) throws SQLException, NullPointerException {
		Statement stmt;
		int result;
		String query1
				= "SELECT S.SET_ID"
				+ " FROM "
				+ schema + ".SETS S "
				+ "WHERE S.USER_ID =" + userID;
		stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rset = stmt.executeQuery ( query1 );
		rset.absolute ( sel_row );
		result = rset.getInt ( 1 );
		stmt.close ();
		rset.close ();
		return result;

	}

	public DBTableModel getFilteredMediaListFromUser ( int id, String title ) throws SQLException, NullPointerException {
		StringMatch stringMatch = new StringMatch ();
		if ( stringMatch.isSqlInjection ( title ) == false ) {
			//rimozioni apici dallo user input
			title = title.replaceAll ( " ' ", " '' " );
			PreparedStatement stmt;
			String query1
					= "SELECT M.TITLE AS TITOLO, M.NUMBER_VIEWS AS VISUALIZZAZIONI, M.STARS AS STELLE "
					+ "FROM "
					+ schema + ".MEDIA M JOIN "
					+ schema + ".USERS U ON  U.USER_ID = M.USER_ID "
					+ " WHERE U.USER_ID = " + id
					+ " AND M.MEDIA_TITLE LIKE ? ";
			stmt = connection.prepareStatement ( query1, ResultSet.TYPE_SCROLL_INSENSITIVE );
			stmt.setString ( 1, title );
			rset = stmt.executeQuery ( query1 );
			DBTableModel dataModel = new DBTableModel ( rset, true );
			return dataModel;
		} else {
			throw (new SQLException ( "input non valido" ));
		}
	}

	/**
	 * aggiorna un record in MEDIA
	 *
	 * @throws SQLException
	 */
	public void updateMediaInfo ( int ID, String title, String caption, String privacy, String safe_rate, String copyright ) throws SQLException {
		StringMatch stringMatch = new StringMatch ();
		if ( stringMatch.isSqlInjection ( title ) == false ) {
			//rimozioni apici dallo user input
			title = title.replaceAll ( " ' ", " '' " );
			caption = caption.replaceAll ( " ' ", " '' " );
			String query = "UPDATE "
					+ schema + ".MEDIA M "
					+ " SET TITLE = ?,  CAPTION = ?, PRIVACY = ?, SAFE_RATE = ?, COPYRIGHT = ? "
					+ " WHERE " + " M.MEDIA_ID = " + ID;
			PreparedStatement stmt = connection.prepareStatement ( query, ResultSet.TYPE_SCROLL_INSENSITIVE );

			stmt.setString ( 1, title );
			stmt.setString ( 2, caption );
			stmt.setString ( 3, privacy );
			stmt.setString ( 4, safe_rate );
			stmt.setString ( 5, copyright );
			stmt.executeUpdate ();
			stmt.close ();
		} else {
			throw (new SQLException ( "input non valido" ));
		}
	}

	/**
	 * recupera i tag per un dato media ed imposta i dati in una tabella
	 *
	 * @param mediaID
	 * @return
	 * @throws SQLException
	 */
	public DBTableModel getMediaTag ( int mediaID ) throws SQLException {
		Statement stmt;
		String query1
				= "SELECT T.TAG_TEXT AS Tag "
				+ " FROM "
				+ schema + ".TAG T "
				+ " WHERE T.MEDIA_ID = " + mediaID
				+ " ORDER BY T.TAG_TEXT";
		stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rset = stmt.executeQuery ( query1 );
		DBTableModel dataModel = new DBTableModel ( rset, true );
		return dataModel;
	}

	/**
	 * esegue una query in media e recupera il record dato un ID
	 *
	 * @param mediaID
	 * @return resulset con i dati del record
	 * @throws SQLException
	 */
	public ResultSet getMediainfo ( int mediaID ) throws SQLException {
		ResultSet rs;
		Statement stmt;
		String query1
				= "SELECT * "
				+ "FROM "
				+ schema + ".MEDIA M "
				+ "WHERE M.MEDIA_ID = " + mediaID;
		stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rs = stmt.executeQuery ( query1 );
		rs.next ();
		return rs;

	}

	/**
	 * elimina un file dalla tabella MEDIA
	 *
	 * @param mediaID
	 * @throws SQLException
	 */
	public void deleteMedia ( int mediaID ) throws SQLException {
		PreparedStatement stmt;
		String query
				= "DELETE FROM "
				+ schema + ".MEDIA M "
				+ "WHERE M.MEDIA_ID = " + mediaID;
		stmt = connection.prepareStatement ( query, ResultSet.TYPE_SCROLL_INSENSITIVE );
		stmt.executeUpdate ();
		stmt.close ();
	}

	public void deleteSelectedTag ( int mediaID, String tagTxt ) throws SQLException {
		PreparedStatement stmt;
		String query
				= "DELETE FROM "
				+ schema + ".TAG T "
				+ " WHERE T.MEDIA_ID = ?"
				+ "AND T.TAG_TEXT = ?";
		stmt = connection.prepareStatement ( query, ResultSet.TYPE_SCROLL_INSENSITIVE );
		stmt.setInt ( 1, mediaID );
		stmt.setString ( 2, tagTxt );
		stmt.executeUpdate ();
		stmt.close ();
	}

	/**
	 * elimina i tag dalla tabella MEDIA per un dato mediaID
	 *
	 * @param mediaID
	 * @throws SQLException
	 */
	public void deleteTag ( int mediaID ) throws SQLException {
		PreparedStatement stmt;
		String query
				= "DELETE FROM "
				+ schema + ".TAG T "
				+ "WHERE T.MEDIA_ID = " + mediaID;
		stmt = connection.prepareStatement ( query, ResultSet.TYPE_SCROLL_INSENSITIVE );
		stmt.executeUpdate ();
		stmt.close ();
	}

	/**
	 * stampa resultset TAG
	 */
	public void printTAG ( int mediaID ) {
		ResultSet rs;
		Statement stmt;
		String query = "SELECT * "
				+ "FROM "
				+ schema + ".TAG T "
				+ " WHERE T.MEDIA_ID = " + mediaID;
		try {
			stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
			rs = stmt.executeQuery ( query );
			System.out.println ( "tabella tag: " );
			while ( rs.next () ) {
				System.out.println ( rs.getInt ( 1 ) + rs.getString ( 2 ) );
			}
		} catch ( SQLException ex ) {
			Logger.getLogger ( FQueries.class.getName () ).log ( Level.SEVERE, null, ex );
		}
	}

	/**
	 * aggiunge un tag
	 */
	public void updateTag ( int mediaID, String tag ) throws SQLException {
		StringMatch stringMatch = new StringMatch ();
		if ( stringMatch.isSqlInjection ( tag ) == false ) {
			//rimozioni apici dallo user input
			tag = tag.replaceAll ( " ' ", " '' " );

			//INSERT INTO table_name 
			//VALUES (value1,value2,value3,...);
			String query = "INSERT INTO " + schema + ".TAG " + " ( MEDIA_ID, TAG_TEXT )"
					+ "VALUES ( ?, ? )";

			PreparedStatement stmt = connection.prepareStatement ( query, ResultSet.TYPE_SCROLL_INSENSITIVE );
			stmt.setInt ( 1, mediaID );
			stmt.setString ( 2, tag );

			stmt.executeUpdate ();
			stmt.close ();
		} else {
			throw (new SQLException ( "input non valido" ));
		}
	}

	public DBTableModel filterUserList ( int userID, String filter ) throws SQLException, NullPointerException {
		StringMatch stringMatch = new StringMatch ();
		if ( stringMatch.isSqlInjection ( filter ) == false ) {
			//rimozioni apici dallo user input
			//filter = filter.replaceAll ( " ' ", " '' " );
			PreparedStatement stmt;
			ResultSet rset1;
			String query1
					= selectMediaList
					+ "FROM "
					+ schema + ".MEDIA M"
					+ " WHERE M.USER_ID = ?"
					+ " AND (REGEXP_LIKE ( M.TITLE , ? ) "
					+ " OR REGEXP_LIKE ( M.CAPTION , ? ) )";

			stmt = connection.prepareStatement ( query1, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
			stmt.setInt ( 1, userID );
			stmt.setString ( 2, filter );
			stmt.setString ( 3, filter );
			rset1 = stmt.executeQuery ();
			DBTableModel dataModel = new DBTableModel ( rset1, true );
			return dataModel;
		} else {
			throw (new SQLException ( "input non valido" ));
		}
	}

	public DBTableModel getSetListFromUser ( int id ) throws SQLException, NullPointerException {
		Statement stmt;
		String query1
				= "SELECT S.SET_ID, S.NAME AS NOME, S.DESCRIPTION AS DESCRIZIONE, NVL (SU.UPLOADED_PHOTO, 0) AS FOTO_CARICATE "
				+ " FROM "
				+ schema + ".SETS S "
				+ " JOIN "
				+ schema + ".USERS U ON  U.USER_ID = S.USER_ID "
				+ " LEFT OUTER JOIN "
				+ " SET_USERS SU ON S.SET_ID = SU.SET_ID "
				+ " WHERE U.USER_ID = " + id
				+ " ORDER BY S.SET_ID ";
		stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rset = stmt.executeQuery ( query1 );
		DBTableModel dataModel = new DBTableModel ( rset, true );
		//System.out.println ( query1 );
		return dataModel;

	}

	public int newSet ( int userID, String name, String description ) throws SQLException, NullPointerException {
		String query = "INSERT INTO "
				+ schema + ".SETS ( USER_ID, NAME, DESCRIPTION ) values ( ? , ? , ? )";

		PreparedStatement stmt = connection.prepareStatement ( query );
		//System.out.println ( query );
		stmt.setInt ( 1, userID );
		stmt.setString ( 2, name );
		stmt.setString ( 3, description );
		stmt.executeUpdate ();
		stmt.close ();
		String query1 = " SELECT MAX (S.SET_ID)"
				+ " FROM " + schema + ".SETS S "
				+ " WHERE S.USER_ID = " + userID;
		Statement stmt1 = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		ResultSet rset = stmt1.executeQuery ( query1 );
		rset.absolute ( 1 );
		int res = rset.getInt ( 1 );
		return res;

	}

	public DBTableModel getMediaFromSet ( int setID ) throws SQLException, NullPointerException {
		Statement stmt;
		String query1
				= " SELECT M.TITLE "
				+ " FROM "
				+ schema + ".MEDIA M "
				+ " JOIN "
				+ schema + ".SETS_CONTENT SC ON M.MEDIA_ID = SC.PHOTO_ID "
				+ " WHERE SC.SET_ID = " + setID
				+ " ORDER BY M.MEDIA_ID";
		stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rset = stmt.executeQuery ( query1 );
		DBTableModel dataModel = new DBTableModel ( rset, true );
		return dataModel;

	}

	public void deleteSet ( int setID ) throws SQLException {
		PreparedStatement stmt;
		String query
				= "DELETE FROM "
				+ schema + ".SETS S "
				+ "WHERE S.SET_ID = " + setID;
		stmt = connection.prepareStatement ( query, ResultSet.TYPE_SCROLL_INSENSITIVE );
		stmt.executeUpdate ();
		stmt.close ();
	}

	public void deleteFromSet ( int mediaID ) throws SQLException {
		PreparedStatement stmt;
		String query
				= "DELETE FROM "
				+ schema + ".SETS_CONTENT S "
				+ "WHERE S.PHOTO_ID = " + mediaID;
		stmt = connection.prepareStatement ( query, ResultSet.TYPE_SCROLL_INSENSITIVE );
		stmt.executeUpdate ();
		stmt.close ();
	}

	public int getMediaID ( int sel_row, int userID, int setID ) throws SQLException, NullPointerException {
		Statement stmt;
		ResultSet rset1;
		int result;
		String query = "SELECT M.MEDIA_ID "
				+ " FROM "
				+ " MEDIA M "
				+ " WHERE M.USER_ID = " + userID;
		String query1 = " AND "
				+ "M.MEDIA_ID NOT IN (";
		String query2 = "SELECT M.MEDIA_ID "
				+ " FROM SETS_CONTENT SC "
				+ " JOIN "
				+ " MEDIA M ON SC.PHOTO_ID = M.MEDIA_ID "
				+ " WHERE M.USER_ID = " + userID
				+ "AND SC.SET_ID = " + setID;
		String query3 = ") "
				+ " ORDER BY M.MEDIA_ID";
		String finalquery = query + query1 + query2 + query3;

		stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rset1 = stmt.executeQuery ( finalquery );

		//printRs ( rset1 );
		//System.out.println ( finalquery );
		rset1.absolute ( sel_row );
		result = rset1.getInt ( 1 );
		stmt.close ();
		rset1.close ();
		return result;
	}

	public int getSetMediaID ( int sel_row, int setID ) throws SQLException, NullPointerException {
		Statement stmt;
		ResultSet rset1;
		int result;
		String query1
				= " SELECT M.MEDIA_ID "
				+ " FROM "
				+ schema + ".MEDIA M "
				+ " JOIN "
				+ schema + ".SETS_CONTENT SC ON M.MEDIA_ID = SC.PHOTO_ID "
				+ " WHERE SC.SET_ID = " + setID
				+ " ORDER BY M.MEDIA_ID";

		stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rset1 = stmt.executeQuery ( query1 );
		rset1.absolute ( sel_row );
		result = rset1.getInt ( 1 );
		stmt.close ();
		rset1.close ();
		return result;
	}

	public ResultSet getSetList ( int userID, int setID ) throws SQLException {
		int x = 0;
		String query = "SELECT M.TITLE "
				+ " FROM "
				+ schema + ".MEDIA M "
				+ " WHERE M.USER_ID = " + userID;
		String query1 = " AND "
				+ "M.MEDIA_ID NOT IN (";
		String query2 = "SELECT M.MEDIA_ID "
				+ " FROM "
				+ schema + ".SETS_CONTENT SC "
				+ " JOIN "
				+ schema + ".MEDIA M ON SC.PHOTO_ID = M.MEDIA_ID "
				+ " WHERE M.USER_ID = " + userID
				+ "AND SC.SET_ID = " + setID;
		String query3 = ") "
				+ " ORDER BY M.MEDIA_ID";
		String finalquery = query + query1 + query2 + query3;
		Statement stmt = fConnection.getConnection ().createStatement ();
		ResultSet rset = stmt.executeQuery ( finalquery );
		//System.out.println ( finalquery );

		return rset;
	}

	public void insertMediaInSet ( int mediaID, int setID ) throws SQLException {
		String query = "INSERT INTO " + schema + ".SETS_CONTENT ( SET_ID, PHOTO_ID ) "
				+ " VALUES ( ? , ? )";
		PreparedStatement stmt = connection.prepareStatement ( query );
		stmt.setInt ( 1, setID );
		stmt.setInt ( 2, mediaID );
		stmt.executeUpdate ();
		stmt.close ();
	}

	public DBTableModel filterSetList ( int userID, String filter ) throws SQLException, NullPointerException {
		StringMatch stringMatch = new StringMatch ();
		if ( stringMatch.isSqlInjection ( filter ) == false ) {
			//rimozioni apici dallo user input
			//filter = filter.replaceAll ( " ' ", " '' " );
			PreparedStatement stmt;
			ResultSet rset1;
			String query1
					= "SELECT S.SET_ID, S.NAME AS NOME, S.DESCRIPTION AS DESCRIZIONE, NVL (SU.UPLOADED_PHOTO, 0) AS FOTO_CARICATE "
					+ " FROM " + schema + ".SETS S "
					+ " LEFT OUTER JOIN "
					+ " SET_USERS SU ON S.SET_ID = SU.SET_ID "
					+ " WHERE S.USER_ID = ? "
					+ " AND (REGEXP_LIKE ( S.NAME , ? ) "
					+ " OR REGEXP_LIKE ( S.DESCRIPTION , ? ) )"
					+ " ORDER BY S.SET_ID ";

			stmt = connection.prepareStatement ( query1, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
			stmt.setInt ( 1, userID );
			stmt.setString ( 2, filter );
			stmt.setString ( 3, filter );
			rset1 = stmt.executeQuery ();
			//System.out.println ( query1 );
			DBTableModel dataModel = new DBTableModel ( rset1, true );
			return dataModel;
		} else {
			throw (new SQLException ( "input non valido" ));
		}
	}

	public void updateSetMeta ( int setID, String title, String description ) throws SQLException {
		String query = " UPDATE " + schema + ".SETS "
				+ " SET NAME = ?, DESCRIPTION = ? "
				+ " WHERE SET_ID = ? ";
		System.out.println ( query );
		PreparedStatement stmt = connection.prepareStatement ( query );
		stmt.setString ( 1, title );
		stmt.setString ( 2, description );
		stmt.setInt ( 3, setID );
		stmt.executeUpdate ();
		stmt.close ();

	}

	public String getUsernameFromId ( int id ) throws SQLException {
		String username = null;
		String query = "SELECT U.SCREEN_NAME FROM " + schema + ".USERS U WHERE U.USER_ID = ?";
		PreparedStatement stmt = connection.prepareStatement ( query );
		stmt.setInt ( 1, id );
		ResultSet rs = stmt.executeQuery ();
		rs.next ();
		username = rs.getString ( 1 );
		stmt.close ();
		rs.close ();
		return username;
	}

	public DBTableModel getPMList ( String query ) throws SQLException, NullPointerException {
		Statement stmt;
		stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rset = stmt.executeQuery ( query );
		DBTableModel dataModel = new DBTableModel ( rset, true );
		return dataModel;

	}

	public void deleteFromPM ( int pmId ) throws SQLException {
		PreparedStatement stmt;
		String query
				= "DELETE FROM "
				+ schema + ".PERSONAL_MESSAGE P"
				+ " WHERE P.MESSAGE_ID = " + pmId;
		stmt = connection.prepareStatement ( query, ResultSet.TYPE_SCROLL_INSENSITIVE );
		stmt.executeUpdate ();
		stmt.close ();
	}

	public void setReadPM ( int pmId ) throws SQLException {
		String query = " UPDATE " + schema + ".PERSONAL_MESSAGE "
				+ " SET READ_FLAG = 1 "
				+ " WHERE MESSAGE_ID = ? ";
		PreparedStatement stmt = connection.prepareStatement ( query );
		stmt.setInt ( 1, pmId );
		stmt.executeUpdate ();
		stmt.close ();
	}

	public int getNewPM ( int userID ) throws SQLException, NullPointerException {
		Statement stmt;
		ResultSet rset1;
		int result;
		String query1
				= " SELECT COUNT (*) "
				+ " FROM " + schema + ".PERSONAL_MESSAGE PM"
				+ " WHERE PM.READ_FLAG = 0 AND PM.USER_TO = " + userID
				+ " ORDER BY PM.PMSG_DATE DESC ";
		//System.out.println ( query1 );
		stmt = connection.createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
		rset1 = stmt.executeQuery ( query1 );
		rset1.absolute ( 1 );
		result = rset1.getInt ( 1 );
		stmt.close ();
		rset1.close ();
		return result;
	}
}
