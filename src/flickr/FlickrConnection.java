/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr;

import java.awt.Component;
import java.io.IOException;
import java.net.SocketException;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.pool.*;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author Oz
 */
public class FlickrConnection {

	private String serverHost = "143.225.117.238";
	private String user = "GRUPPO15";
	private String password = "Pass01_Pass01";
	private String service = "xe";
	private String port = "1521";
	private String schema = "GRUPPO15";
	private String httpHost = "flickrdb.altervista.org";
	FlickrFunctions f = new FlickrFunctions ();
	Connection connection = null;
	OracleDataSource ods = null;
	private Component frame;
	private FTPClient ftpClient = null;
	private boolean start = true;

	/**
	 * costruttore per connessione con parametri default
	 *
	 * @throws SQLException
	 */
	public FlickrConnection () throws SQLException {
		openConnection ( serverHost, port, service, user, password );
	}

	/**
	 * costruttore per connessione con parametri impostati da utente
	 *
	 * @param host indirizzo del server
	 * @param port porta del servizio
	 * @param service tipo servizio
	 * @param user nome utente
	 * @param password password
	 * @param schema schema
	 * @throws java.sql.SQLException
	 */
	public FlickrConnection ( String host, String port, String service, String user, String password, String schema ) throws SQLException, NumberFormatException {
		Integer.parseInt ( port );
		this.serverHost = host;
		this.user = user;
		this.password = password;
		this.service = service;
		this.port = port;
		this.schema = schema;

		openConnection ( host, port, service, user, password );
		//mantiene in vita la connessione
		///newThread ();
	}

	/**
	 * restituisce lo schema del db
	 *
	 * @return
	 */
	public final String getSchema () {
		return schema;
	}

	/**
	 * restituisce un oggetto Connection contenente la connessione
	 *
	 * @return
	 */
	public final Connection getConnection () {
		return connection;
	}

	/**
	 * apre la connessione al server oracle ( driver thin )
	 *
	 * @param host
	 * @param port
	 * @param service
	 * @param user
	 * @param password
	 * @throws SQLException
	 */
	public final void openConnection ( String host, String port, String service, String user, String password ) throws SQLException {

		String url = "jdbc:oracle:thin:@//" + host + ":" + port + "/" + service;
		System.out.println ( url );

		this.ods = new OracleDataSource ();
		ods.setURL ( url );
		ods.setUser ( user );
		ods.setPassword ( password );
		connection = ods.getConnection ();
		//System.out.println ( "connessione effettuata!" );

	}

	/**
	 * chiude la connessione corrente
	 */
	public void closeConnection () {

		try {
			connection.close ();
		} catch ( SQLException ex ) {
			Logger.getLogger ( FlickrConnection.class.getName () ).log ( Level.SEVERE, null, ex );
		}

	}

	/**
	 * controlla se la connessione e' aperta. se la connesione è chiusa tenta
	 * una nuova connessione. se fallisce, restituisce falso
	 *
	 * @return true se la connessione ad Oracle è aperta.
	 */
	public boolean isOpen () {

		int i = 0;
		boolean res = false;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = connection.createStatement ();
			rs = stmt.executeQuery ( "SELECT SYSDATE FROM Dual" );
			rs.next ();
			res = true;
		} catch ( SQLException e ) {
			try {
				openConnection ( serverHost, port, service, user, password );
				res = true;
			} catch ( SQLException ex ) {
				Logger.getLogger ( FlickrConnection.class.getName () ).log ( Level.SEVERE, null, ex );
			}

		} finally {
			if ( stmt != null ) {
				try {
					stmt.close ();
				} catch ( SQLException ex ) {
					Logger.getLogger ( FlickrConnection.class.getName () ).log ( Level.SEVERE, null, ex );
				}
			}
			if ( rs != null ) {
				try {
					rs.close ();
				} catch ( SQLException ex ) {
					Logger.getLogger ( FlickrConnection.class.getName () ).log ( Level.SEVERE, null, ex );
				}
			}
		}
		return res;
	}

	/**
	 * conta i media caricati sul db, raggruppati per utente
	 *
	 * @throws SQLException
	 */
	public void QueryUserList () throws SQLException {
		Statement stmt;
		stmt = connection.createStatement ();
		ResultSet rset = stmt.executeQuery ( "SELECT COUNT(MEDIA_ID) FROM" + schema + ".MEDIA GROUP BY USER_ID" );
		/*
		 while ( rset.next () ) {
		 System.out.println ( rset.getString ( "SCREEN_NAME" ) );
		 }
		 */
		FUtenteLista u = new FUtenteLista ( rset );
		u.setVisible ( true );
		stmt.close ();
	}

	/**
	 * seleziona tutti gli utenti
	 */
	public ResultSetMetaData getResultSetMetaData () throws SQLException {
		Statement stmt;
		ResultSet rset = null;
		ResultSetMetaData rsMeta = null;

		stmt = connection.createStatement ();
		rset = stmt.executeQuery ( "SELECT * FROM " + schema + ".USERS" );
		rsMeta = rset.getMetaData ();
		stmt.close ();

		return rsMeta;
	}

	public void returnRS ( ResultSet rset ) throws SQLException {
		Statement stmt;
		stmt = connection.createStatement ();
		rset = stmt.executeQuery ( "SELECT * FROM " + schema + ".USERS" );
		stmt.close ();
		rset.close ();
	}

	/**
	 * *
	 * gestione connessione ftp Host: ftp.flickrdb.altervista.org Porta: 21
	 * Utente: gruppo15 Pass: Pass01_Pass01
	 */
	public FTPClient getFTPclient () throws SocketException {
		boolean test = ftpOpenConnection ();
		if ( test ) {
			return ftpClient;
		} else {
			throw new SocketException ();
		}
	}

	/**
	 * apre una connessione ftp
	 *
	 * @return successo o insuccesso
	 */
	public boolean ftpOpenConnection () {

		boolean login = false;

		String host = "ftp.flickrdb.altervista.org";

		String user = "gruppo15@flickrdb";
		String password = "Pass01_Pass01";
		ftpClient = new FTPClient ();

		try {

			ftpClient.connect ( host, 21 );
			login = ftpClient.login ( user, password );
			ftpClient.setFileType ( FTP.BINARY_FILE_TYPE );
			ftpClient.enterLocalPassiveMode ();
		} catch ( SocketException ex ) {
			System.out.println ( "errore socket: " + ex.getMessage () );
			System.out.println ( ex.getMessage () );

			if ( login == false ) {
				ftpClient.disconnect ();
			}
		} catch ( IOException ex ) {
			System.out.println ( "errore IO: " + ex.getMessage () );
			if ( login == false ) {
				ftpClient.disconnect ();
			}
		} finally {
			return login;
		}

	}

	/**
	 * chiude la connessione ftp
	 *
	 * @return
	 */
	public boolean ftpCloseConnection () {

		boolean test = false;
		try {
			ftpClient.logout ();
			ftpClient.disconnect ();
			ftpClient = null;
			test = true;
		} catch ( IOException ex ) {
			System.out.println ( "Errore nella connessione ftp: " + ex.getMessage () );
			//Logger.getLogger ( FlickrConnection.class.getName () ).log ( Level.SEVERE, null, ex );
		} finally {
			return test;
		}
	}

	/**
	 * imposta il nuovo host http
	 *
	 * @param newHost
	 */
	public void setHttpHost ( String newHost ) {
		httpHost = newHost;
	}

	/**
	 * restituisce l'host http corrente
	 *
	 * @return
	 */
	public String getHttpHost () {
		return httpHost;
	}
}
