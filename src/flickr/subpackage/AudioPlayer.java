/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr.subpackage;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.*;

/**
 * This is an example program that demonstrates how to play back an audio file
 * using the Clip in Java Sound API.
 *
 * @author www.codejava.net
 *
 */
public class AudioPlayer implements LineListener {

	/**
	 * this flag indicates whether the playback completes or not.
	 */
	boolean playCompleted;
	private Clip audioClip = null;

	public AudioPlayer () {
		String audioFilePath = "./src/flickr/res/audio/starwars.wav";
		play ( audioFilePath );
	}

	/**
	 * Play a given audio file.
	 *
	 * @param audioFilePath Path of the audio file.
	 */
	void play ( String audioFilePath ) {
		File audioFile = new File ( audioFilePath );
		try {
			AudioInputStream audioStream = AudioSystem.getAudioInputStream ( audioFile );
			AudioFormat format = audioStream.getFormat ();
			DataLine.Info info = new DataLine.Info ( Clip.class, format );
			audioClip = (Clip) AudioSystem.getLine ( info );
			audioClip.addLineListener ( this );
			audioClip.open ( audioStream );
			audioClip.start ();
			audioClip.loop ( 99 );
		} catch ( UnsupportedAudioFileException ex ) {
			System.out.println ( "The specified audio file is not supported." );
			ex.printStackTrace ();
		} catch ( LineUnavailableException ex ) {
			System.out.println ( "Audio line for playing back is unavailable." );
			ex.printStackTrace ();
		} catch ( IOException ex ) {
			System.out.println ( "Error playing the audio file." );
			ex.printStackTrace ();
		}
	}

	public Clip getAudio () {
		return audioClip;
	}

	public void stopAudio ( Clip music ) {
		try {
			music.stop ();
		} catch ( NullPointerException ex ) {
			System.out.println ( "Non c'è nessuna musica da stoppare.\n" );
		}
	}

	/**
	 * Listens to the START and STOP events of the audio line.
	 */
	@Override
	public void update ( LineEvent event ) {
		LineEvent.Type type = event.getType ();
//		if ( type == LineEvent.Type.START ) {
//			System.out.println ( "Playback started." );
//		} else if ( type == LineEvent.Type.STOP ) {
//			playCompleted = true;
//			System.out.println ( "Playback completed." );
//		}
	}
}
