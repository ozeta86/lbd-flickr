/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr.subpackage;

/**
 *
 * @author Eddy
 */
public class StringMatch {

	/**
	 * Attributi che contengono i pattern di "accettazione" per l'username, la
	 * password e il nome dell'utente.
	 */
	private static final String usernameMatch = "^[a-zA-Z]+[0-9]*";
	private static final String passwordMatch = "^(?=.*\\d)(?=.*[A-Z])[^ ]*$";
	private static final String nameMatch = "[A-Za-z]+[ A-Za-z]*[a-z]$";
	private static final String emailMatch = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * Metodo che confronta l'username passato in input col suo pattern di
	 * "accettazione".
	 *
	 * @param username di tipo String
	 * @return booleano
	 */
	public boolean isUsernameCorrect ( String username ) {
		return username.matches ( usernameMatch );
	}

	/**
	 * Metodo che confronta la password passata in input col suo pattern di
	 * "accettazione".
	 *
	 * @param password di tipo String
	 * @return booleano
	 */
	public boolean isPasswordCorrect ( String password ) {
		return (password.matches ( passwordMatch )
				&& password.length () >= 8);
	}

	/**
	 * Metodo che confronta il nome passato in input col suo pattern di
	 * "accettazione".
	 *
	 * @param name di tipo String
	 * @return booleano
	 */
	public boolean isNameCorrect ( String name ) {
		return name.matches ( nameMatch );
	}

	/**
	 * Metodo che confronta l'email passata in input col suo pattern di
	 * "accettazione".
	 *
	 * @param email di tipo String
	 * @return booleano
	 */
	public boolean isEmailCorrect ( String email ) {
		return email.matches ( emailMatch );
	}

	/**
	 * Metodo che controlla se ci sono apici nella stringa passata in input.
	 *
	 * @param string di tipo String
	 * @return booleano
	 */
	public boolean areThereApexsInString ( String string ) {
		boolean flag = false;
		char[] test = string.toCharArray ();
		int i = 0;
		while ( i < string.length () && !flag ) {
			if ( test[i] == '\'' || test[i] == '\"' ) {
				flag = true;
			}
			i++;
		}
		return flag;
	}

	/**
	 * Metodo che controlla se la stringa passata in input contiene del codice
	 * SQL che permetta SQL Injection.
	 *
	 * @param string di tipo String
	 * @return booleano
	 */
	public boolean isSqlInjection ( String string ) {
		boolean flag = false;
		char[] test = string.toCharArray ();
		int i = 0, j = 3;
		while ( j < string.length () && !flag ) {
			if ( test[i] == ';' || (test[i] == '\'' && test[i + 1] == ' '
					&& (test[j - 1] == 'O' || test[j - 1] == 'o')
					&& (test[j] == 'R' || test[j] == 'r')) ) {
				flag = true;
			}
			i++;
			j++;
		}
		return flag;
	}

	/**
	 * Metodo che ripulisce i tag relativi a un file caricato sul database di
	 * spazi e virgole e li inserisce in un array di stringhe.
	 *
	 * @param tag di tipo String
	 * @return tags di tipo String[]
	 */
	public String[] cleanTag ( String tag ) {
		tag = tag.replaceAll ( "[ ]", "" );
		System.out.println ( tag );
		int i = 0;
		String[] tags = new String[20];
		java.util.StringTokenizer st = new java.util.StringTokenizer ( tag, "," );
		while ( st.hasMoreTokens () ) {
			tags[i++] = st.nextToken ();
		}
		return tags;
	}

}
