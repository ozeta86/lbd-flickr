/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr.subpackage;

/**
 *
 * @author Eddy
 */
public class NoRowTableSelectedException extends RuntimeException {

	/**
	 * Costruttore privo di parametri in ingresso che richiama il costruttore
	 * della superclasse.
	 */
	public NoRowTableSelectedException () {
		super ();
	}

	/**
	 * @Overloading Costruttore che ha un parametro in ingresso e che viene
	 * passato al costruttore della superclasse.
	 *
	 * @param message di tipo String
	 */
	public NoRowTableSelectedException ( String message ) {
		super ( message );
	}
}
