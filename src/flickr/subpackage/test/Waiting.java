/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr.subpackage.test;

/**
 *
 * @author Eddy
 */
public class Waiting extends Message {

	/**
	 * Creates new form Attesa
	 */
	/**
	 * Costruttore che inizializza le sue componenti, setta non visibile il
	 * pulsante appartenente alla superclasse e aggiunge un messaggio di attesa
	 * nella label.
	 */
	public Waiting () {
		initComponents ();
		jButton1.setVisible ( false );
		jLabel1.setText ( "Attendere prego." );
	}

	/**
	 * @Overloading Costruttore che inizializza le sue componenti, setta non
	 * visibile il pulsante appartenente alla superclasse e aggiunge un
	 * messaggio di attesa concatenato ad un altro messaggio passato in input
	 * nella label.
	 */
	public Waiting ( String str ) {
		initComponents ();
		jButton1.setVisible ( false );
		jLabel1.setText ( str + " Attendere prego." );
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings ("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jProgressBar1 = new javax.swing.JProgressBar();
        jLabel1 = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 248, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	/**
	 * Setta il valore della progress bar col valore passato in input.
	 *
	 * @param value di tipo int
	 */
	public void setProgressValue ( int value ) {
		jProgressBar1.setValue ( value );
	}

	/**
	 * @param args the command line arguments
	 */
//	public static void main ( String args[] ) {
//		java.awt.EventQueue.invokeLater ( new Runnable () {
//			public void run () {
//				new Waiting ().setVisible ( true );
//			}
//		} );
//	}
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JProgressBar jProgressBar1;
    // End of variables declaration//GEN-END:variables
}
