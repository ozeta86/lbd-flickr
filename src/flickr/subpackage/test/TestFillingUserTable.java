/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr.subpackage.test;

import flickr.FlickrFunctions;
import java.awt.Component;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.pool.*;

/**
 *
 * @author Eddy
 */
public class TestFillingUserTable {

	private String defaultServerHost = "143.225.117.238";
	private String defaultUser = "GRUPPO15";
	private String defaultPassword = "Pass01_Pass01";
	private String service = "xe";
	private String port = "1521";
	private String schema = "flickr";
	FlickrFunctions f = new FlickrFunctions ();
	Connection connection = null;
	OracleDataSource ods = null;
	private Component frame;

	public TestFillingUserTable () throws SQLException {
		openConnection ( defaultServerHost, port, service, defaultUser, defaultPassword );
	}

	public TestFillingUserTable ( String host, String port, String service, String user, String password, String schema ) throws SQLException, NumberFormatException {
		Integer.parseInt ( port );
		openConnection ( host, port, service, user, password );
		this.schema = schema;
		QueryUserList ();
	}

	public final void openConnection ( String host, String port, String service, String user, String password ) throws SQLException {

		Waiting a = new Waiting ( "Connessione in corso." );
		a.setVisible ( true );
		String url = "jdbc:oracle:thin:@//" + host + ":" + port + "/" + service;
		System.out.println ( url );
		this.ods = new OracleDataSource ();
		ods.setURL ( url );
		ods.setUser ( user );
		ods.setPassword ( password );
		for ( int i = 100000000; i < 76000000; i++ ) {
			a.setProgressValue ( i / 100000000 );
		}
		connection = ods.getConnection ();
		for ( int i = 76000000; i < 101000000; i++ ) {
			a.setProgressValue ( i / 1000000 );
		}
		a.dispose ();
		//System.out.println ( "connessione effettuata!" );

	}

	public void closeConnection () {
		try {
			connection.close ();
		} catch ( SQLException ex ) {
			Logger.getLogger ( TestFillingUserTable.class.getName () ).log ( Level.SEVERE, null, ex );
		}
	}

	public void QueryUserList () throws SQLException {
		String str1 = "SELECT USERS.SCREEN_NAME, USERS.EMAIL, NFOTO.N_FOTO, NSET.N_SET, NPM.N_PM, NFW.N_FW";
		String str2 = " FROM USERS";
		String str3 = " LEFT JOIN NFOTO";
		String str4 = " ON USERS.USER_ID = NFOTO.USER_ID";
		String str5 = " LEFT JOIN NSET";
		String str6 = " ON USERS.USER_ID = NSET.USER_ID";
		String str7 = " LEFT JOIN NPM";
		String str8 = " ON USERS.USER_ID = NPM.USER_TO";
		String str9 = " LEFT JOIN NFW";
		String str10 = " ON USERS.USER_ID = NFW.ID_TO";
		String query = str1 + str2 + str3 + str4 + str5 + str6 + str7 + str8 + str9 + str10;
		Statement stmt;
		stmt = connection.createStatement ();
		ResultSet rset = stmt.executeQuery ( query );
		FUtenteLista u = new FUtenteLista ( rset );
		u.setVisible ( true );
		stmt.close ();
	}

}
