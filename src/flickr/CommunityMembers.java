/*
 * Progetto Flickr 2014
 * Laboratorio di Basi di Dati
 * Gruppo 15
 * Marco Carrozzo				N861240
 * Eddy Pasquale Marino			N861133
 */
package flickr;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Eddy
 */
public class CommunityMembers extends FTable {

	/**
	 * Attributi relativi alla connessione e alla query
	 */
	FlickrFunctions fObj = new FlickrFunctions ();
	ResultSet rs = null;
	ResultSetMetaData rsMeta = null;

	/**
	 * Creates new form CommunityMembers
	 */
	/**
	 * Costruttore che crea una nuova connessione al database, inizializza le
	 * componenti e mostra la tabella contente i membri delle community inseriti
	 * nel database. Può sollevare eccezioni.
	 */
	public CommunityMembers () {
		initComponents ();
		try {
			this.connection = new FlickrConnection ();
		} catch ( SQLException ex ) {
			if ( ex.getMessage ().contains ( "The Network Adapter could not establish the connection" ) ) {
				noConnection ();
			} else {
				System.out.println ( "errore" );
				Logger.getLogger ( CommunityMembers.class.getName () ).log ( Level.SEVERE, null, ex );
			}
		}
		if ( connection != null ) {
			initComponents ();
			showTable ();
		}
	}

	/**
	 * @Overloading Costruttore che pone il valore della variabile locale
	 * connection uguale a quello della variabile passata in input, setta le sue
	 * componenti e mostra la tabella.
	 * @param connection di tipo FlickrConnection
	 */
	public CommunityMembers ( FlickrConnection connection ) {
		this.connection = connection;
		initComponents ();
		showTable ();
	}

	/**
	 * @Overloading Costruttore che, dopo aver posto il valore dell'attributo
	 * locale connection uguale a quello passato in input e inizializzato le sue
	 * componenti, mostra nella tabella i membri della community passata in
	 * input.
	 *
	 * @param connection di tipo FlickrConnection
	 * @param communityId di tipo BigDecimal
	 * @param nameCommunity di tipo String
	 */
	public CommunityMembers ( FlickrConnection connection, BigDecimal communityId, String nameCommunity ) {
		this.connection = connection;
		initComponents ();
		showTable ( communityId, nameCommunity );
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings ("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Flickr: membri della Community");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 800, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	/**
	 * Metodo che mostra tutti i membri delle community presenti nel database.
	 * Può sollevare eccezioni.
	 */
	private void showTable () {
		super.setTitle2 ( "Membri Community: Tutti" );
		Statement stmt;
		String query = "SELECT CD.COMMUNITY_ID as ID, U.SCREEN_NAME as MEMBRO, CD.USER_ROLE as RUOLO "
				+ "FROM " + connection.getSchema () + ".COMMUNITY_DATA CD "
				+ "JOIN " + connection.getSchema () + ".USERS U "
				+ "ON CD.MEMBER_ID = U.USER_ID "
				+ "ORDER BY ID, RUOLO";
		try {
			stmt = connection.getConnection ().createStatement ( ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
			ResultSet rset;
			rset = stmt.executeQuery ( query );
			DBTableModel dataModel = new DBTableModel ( rset, true );
			jTable1.setModel ( dataModel );
			jPanel1.setVisible ( true );
			jScrollPane1.setVisible ( true );
			jTable1.setVisible ( true );
		} catch ( SQLException ex ) {
			Logger.getLogger ( CommunityMembers.class.getName () ).log ( Level.SEVERE, null, ex );
		} catch ( NullPointerException ex ) {
			noConnection ();
		}
	}

	/**
	 * @Overloading Metodo che mostra, nella tabella, tutti i membri
	 * appartenenti alla community passata in input. Può sollevare eccezioni.
	 *
	 * @param communityId di tipo BigDecimal
	 * @param name di tipo String
	 */
	private void showTable ( BigDecimal communityId, String name ) {
		super.setTitle2 ( name );
		PreparedStatement stmt;
		String query = "SELECT U.USER_ID as ID, U.SCREEN_NAME as MEMBRO, CD.USER_ROLE as RUOLO "
				+ "FROM " + connection.getSchema () + ".COMMUNITY_DATA CD "
				+ "JOIN " + connection.getSchema () + ".USERS U "
				+ "ON CD.MEMBER_ID = U.USER_ID "
				+ "WHERE CD.COMMUNITY_ID = ? "
				+ "ORDER BY ID,RUOLO";
		try {
			stmt = connection.getConnection ().prepareStatement ( query,
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY );
			stmt.setBigDecimal ( 1, communityId );
			ResultSet rset = stmt.executeQuery ();
			DBTableModel dataModel = new DBTableModel ( rset, true );
			jTable1.setModel ( dataModel );
			jPanel1.setVisible ( true );
			jScrollPane1.setVisible ( true );
			jTable1.setVisible ( true );
		} catch ( SQLException ex ) {
			Logger.getLogger ( CommunityMembers.class.getName () ).log ( Level.SEVERE, null, ex );
		} catch ( NullPointerException ex ) {
			noConnection ();
		}
	}

	/**
	 * @param args the command line arguments
	 */
//	public static void main ( String args[] ) {
//		java.awt.EventQueue.invokeLater ( new Runnable () {
//			public void run () {
//				new CommunityMembers ().setVisible ( true );
//			}
//		} );
//	}
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
